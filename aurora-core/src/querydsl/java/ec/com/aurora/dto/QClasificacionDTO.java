package ec.com.aurora.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QClasificacionDTO is a Querydsl query type for ClasificacionDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QClasificacionDTO extends EntityPathBase<ClasificacionDTO> {

    private static final long serialVersionUID = 382745355L;

    public static final QClasificacionDTO clasificacionDTO = new QClasificacionDTO("clasificacionDTO");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombreclasificacion = createString("nombreclasificacion");

    public QClasificacionDTO(String variable) {
        super(ClasificacionDTO.class, forVariable(variable));
    }

    public QClasificacionDTO(Path<? extends ClasificacionDTO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QClasificacionDTO(PathMetadata metadata) {
        super(ClasificacionDTO.class, metadata);
    }

}

