package ec.com.aurora.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QArticuloDTO is a Querydsl query type for ArticuloDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QArticuloDTO extends EntityPathBase<ArticuloDTO> {

    private static final long serialVersionUID = -176911068L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QArticuloDTO articuloDTO = new QArticuloDTO("articuloDTO");

    public final QCatalogoDTO catalogo;

    public final QClasificacionDTO clasificacionproducto;

    public final NumberPath<java.math.BigDecimal> existencia = createNumber("existencia", java.math.BigDecimal.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> idCatalogo = createNumber("idCatalogo", Integer.class);

    public final NumberPath<Integer> idClasificacion = createNumber("idClasificacion", Integer.class);

    public final StringPath nombreproducto = createString("nombreproducto");

    public final NumberPath<java.math.BigDecimal> precio = createNumber("precio", java.math.BigDecimal.class);

    public QArticuloDTO(String variable) {
        this(ArticuloDTO.class, forVariable(variable), INITS);
    }

    public QArticuloDTO(Path<? extends ArticuloDTO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QArticuloDTO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QArticuloDTO(PathMetadata metadata, PathInits inits) {
        this(ArticuloDTO.class, metadata, inits);
    }

    public QArticuloDTO(Class<? extends ArticuloDTO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.catalogo = inits.isInitialized("catalogo") ? new QCatalogoDTO(forProperty("catalogo")) : null;
        this.clasificacionproducto = inits.isInitialized("clasificacionproducto") ? new QClasificacionDTO(forProperty("clasificacionproducto")) : null;
    }

}

