package ec.com.aurora.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVentaArticuloDTO is a Querydsl query type for VentaArticuloDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVentaArticuloDTO extends EntityPathBase<VentaArticuloDTO> {

    private static final long serialVersionUID = -968285448L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVentaArticuloDTO ventaArticuloDTO = new QVentaArticuloDTO("ventaArticuloDTO");

    public final QArticuloDTO articulo;

    public final NumberPath<java.math.BigDecimal> cantidad = createNumber("cantidad", java.math.BigDecimal.class);

    public final StringPath comprador = createString("comprador");

    public final StringPath email = createString("email");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QVentaArticuloDTO(String variable) {
        this(VentaArticuloDTO.class, forVariable(variable), INITS);
    }

    public QVentaArticuloDTO(Path<? extends VentaArticuloDTO> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVentaArticuloDTO(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVentaArticuloDTO(PathMetadata metadata, PathInits inits) {
        this(VentaArticuloDTO.class, metadata, inits);
    }

    public QVentaArticuloDTO(Class<? extends VentaArticuloDTO> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.articulo = inits.isInitialized("articulo") ? new QArticuloDTO(forProperty("articulo"), inits.get("articulo")) : null;
    }

}

