package ec.com.aurora.dto;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCatalogoDTO is a Querydsl query type for CatalogoDTO
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCatalogoDTO extends EntityPathBase<CatalogoDTO> {

    private static final long serialVersionUID = 2028508329L;

    public static final QCatalogoDTO catalogoDTO = new QCatalogoDTO("catalogoDTO");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombrecatalogo = createString("nombrecatalogo");

    public QCatalogoDTO(String variable) {
        super(CatalogoDTO.class, forVariable(variable));
    }

    public QCatalogoDTO(Path<? extends CatalogoDTO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCatalogoDTO(PathMetadata metadata) {
        super(CatalogoDTO.class, metadata);
    }

}

