/**
 * 
 */
package ec.com.aurora.gestor.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.querydsl.core.QueryException;

import ec.com.aurora.commons.exception.AuroraException;
import ec.com.aurora.commons.exception.AuroraStateException;
import ec.com.aurora.dao.ClasificacionDao;
import ec.com.aurora.dto.ClasificacionDTO;
import ec.com.aurora.gestor.ClasificacionGestor;

/**
 * @author amaru
 *
 */
@Stateless
public class ClasificacionGestorImpl implements ClasificacionGestor {

	@EJB
	private ClasificacionDao clasificacionDao;

	@Override
	public List<ClasificacionDTO> obtenerClasificacioes() {
		try {
			List<ClasificacionDTO> clasificaciones = clasificacionDao.obtenerClasificacioes();
			if (clasificaciones.isEmpty()) {
				throw new AuroraStateException("No existen clasificaicones registradas.");
			}
			return clasificaciones;
		} catch (QueryException e) {
			throw new AuroraException("No fue posible obtener las clasificaciones del producto.", e);
		}
	}

}
