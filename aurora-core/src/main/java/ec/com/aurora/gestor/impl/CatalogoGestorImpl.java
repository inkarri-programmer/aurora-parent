/**
 * 
 */
package ec.com.aurora.gestor.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.HibernateException;

import com.querydsl.core.QueryException;

import ec.com.aurora.commons.exception.AuroraException;
import ec.com.aurora.commons.exception.AuroraStateException;
import ec.com.aurora.dao.CatalogoDao;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.gestor.CatalogoGestor;

/**
 * @author amaru
 *
 */
@Stateless
public class CatalogoGestorImpl implements CatalogoGestor {

	@EJB
	private CatalogoDao catalogoDao;

	@Override
	public List<CatalogoDTO> obtenerCatalogos() {
		try {
			List<CatalogoDTO> catalogos = catalogoDao.obtenerCatalogos();
			if (catalogos.isEmpty()) {
				throw new AuroraStateException("No existen catalogos registradas.");
			}
			return catalogos;
		} catch (QueryException e) {
			throw new AuroraException("No fue posible obtener catalogos del producto.", e);
		}
	}

	@Override
	public void crearCatalogos(CatalogoDTO catalogo) {
		try {
			if (catalogo == null) {
				throw new AuroraException("Los datos del nuevo catalogo son obligatorios.");
			}
			catalogoDao.crearCatalogos(catalogo);
		} catch (HibernateException e) {
			throw new AuroraException("No fue posible registrar una nueva categoria.");
		}

	}

}
