/**
 * 
 */
package ec.com.aurora.gestor;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.ClasificacionDTO;

/**
 * @author amaru
 *
 */
@Local
public interface ClasificacionGestor {

	List<ClasificacionDTO> obtenerClasificacioes();

}
