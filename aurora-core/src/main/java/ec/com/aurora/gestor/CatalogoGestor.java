/**
 * 
 */
package ec.com.aurora.gestor;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.CatalogoDTO;

/**
 * @author amaru
 *
 */
@Local
public interface CatalogoGestor {

	List<CatalogoDTO> obtenerCatalogos();

	void crearCatalogos(CatalogoDTO catalogo);
	
}
