/**
 * 
 */
package ec.com.aurora.gestor.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.HibernateException;

import com.querydsl.core.QueryException;

import ec.com.aurora.commons.exception.AuroraException;
import ec.com.aurora.commons.exception.AuroraStateException;
import ec.com.aurora.dao.ArticuloDao;
import ec.com.aurora.dto.ArticuloDTO;
import ec.com.aurora.gestor.ArticuloGestor;

/**
 * @author amaru
 *
 */
@Stateless
public class ArticuloGestorImpl implements ArticuloGestor {

	@EJB
	private ArticuloDao articuloDao;

	@Override
	public void agregarArticulo(ArticuloDTO articulo) {
		try {
			if (articulo == null) {
				throw new AuroraException("Los datos de articulo son obligatorios para realizar el proceso.");
			}
			articuloDao.agregarArticulo(articulo);
		} catch (HibernateException e) {
			throw new AuroraException("No fue posible registrar el articulo.");
		}
	}

	@Override
	public void actualizarArticulo(ArticuloDTO articulo) {
		try {
			if (articulo == null) {
				throw new AuroraException("Los datos de articulo son obligatorios para realizar la actualizacion.");
			}
			articuloDao.actualizarArticulo(articulo);
		} catch (HibernateException e) {
			throw new AuroraException("No fue posible actualizar el articulo.", e);
		}
	}

	@Override
	public List<ArticuloDTO> obtnerArticulos() {
		try {
			List<ArticuloDTO> articulos = articuloDao.obtnerArticulos();
			if (articulos.isEmpty()) {
				throw new AuroraStateException("Aun no existen artculos registrados.");
			}
			return articulos;
		} catch (QueryException e) {
			throw new AuroraException("No fue posible obtener la lista de articulos.");
		}
	}

	@Override
	public ArticuloDTO obtnerArticuloPorId(int idArticulo) {
		try {
			ArticuloDTO articulo = articuloDao.obtnerArticuloPorId(idArticulo);
			if (articulo == null) {
				throw new AuroraStateException("El articulo que busca no existe.");
			}
			return articulo;
		} catch (QueryException e) {
			throw new AuroraException("No fue posible obtener lel articulo.");
		}
	}

}
