/**
 * 
 */
package ec.com.aurora.servicio.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ec.com.aurora.dto.ArticuloDTO;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.dto.ClasificacionDTO;
import ec.com.aurora.gestor.ArticuloGestor;
import ec.com.aurora.gestor.CatalogoGestor;
import ec.com.aurora.gestor.ClasificacionGestor;
import ec.com.aurora.servicio.KardexService;

/**
 * @author amaru
 *
 */
@Stateless
public class KardexServiceImpl implements KardexService {

	@EJB
	private ArticuloGestor articuloGestor;

	@EJB
	private CatalogoGestor catalogoGestor;

	@EJB
	private ClasificacionGestor clasificacionGestor;

	@Override
	public void agregarArticulo(ArticuloDTO articulo) {
		articuloGestor.agregarArticulo(articulo);
	}

	@Override
	public void actualizarArticulo(ArticuloDTO articulo) {
		articuloGestor.actualizarArticulo(articulo);
	}

	@Override
	public List<ArticuloDTO> obtnerArticulos() {
		return articuloGestor.obtnerArticulos();
	}

	@Override
	public ArticuloDTO obtnerArticuloPorId(int idArticulo) {
		return articuloGestor.obtnerArticuloPorId(idArticulo);
	}

	@Override
	public List<CatalogoDTO> obtenerCatalogos() {
		return catalogoGestor.obtenerCatalogos();
	}

	@Override
	public void crearCatalogos(CatalogoDTO catalogo) {
		catalogoGestor.crearCatalogos(catalogo);
	}

	@Override
	public List<ClasificacionDTO> obtenerClasificacioes() {
		return clasificacionGestor.obtenerClasificacioes();
	}

}
