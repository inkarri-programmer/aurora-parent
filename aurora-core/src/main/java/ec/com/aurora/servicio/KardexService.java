/**
 * 
 */
package ec.com.aurora.servicio;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.ArticuloDTO;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.dto.ClasificacionDTO;

/**
 * @author amaru
 *
 */
@Local
public interface KardexService {

	void agregarArticulo(ArticuloDTO articulo);

	void actualizarArticulo(ArticuloDTO articulo);

	List<ArticuloDTO> obtnerArticulos();

	ArticuloDTO obtnerArticuloPorId(int idArticulo);
	
	List<CatalogoDTO> obtenerCatalogos();

	void crearCatalogos(CatalogoDTO catalogo);
	
	List<ClasificacionDTO> obtenerClasificacioes();

}
