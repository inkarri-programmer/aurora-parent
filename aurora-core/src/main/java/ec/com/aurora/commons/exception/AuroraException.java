/**
 * 
 */
package ec.com.aurora.commons.exception;

import javax.ejb.ApplicationException;

/**
 * @author amaru
 *
 */
@ApplicationException(rollback = true)
public class AuroraException extends RuntimeException {

	private static final long serialVersionUID = -1784180190395806924L;

	public AuroraException() {
	}

	public AuroraException(String mensaje) {
		super(mensaje);
	}

	public AuroraException(Throwable causa) {
		super(causa);
	}

	public AuroraException(String mensaje, Throwable causa) {
		super(mensaje, causa);
	}
}
