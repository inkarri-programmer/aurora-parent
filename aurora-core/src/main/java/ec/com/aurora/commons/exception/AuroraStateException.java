/**
 * 
 */
package ec.com.aurora.commons.exception;

import javax.ejb.ApplicationException;

/**
 * @author amaru
 *
 */
@ApplicationException(rollback = true)
public class AuroraStateException extends RuntimeException {

	private static final long serialVersionUID = 3363657600173246011L;

	public AuroraStateException() {
	}

	public AuroraStateException(String mensaje) {
		super(mensaje);
	}

	public AuroraStateException(Throwable causa) {
		super(causa);
	}

	public AuroraStateException(String mensaje, Throwable causa) {
		super(mensaje, causa);
	}
	
}

