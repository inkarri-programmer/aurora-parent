/**
 * 
 */
package ec.com.aurora.dao.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.querydsl.jpa.impl.JPAQuery;

import ec.com.aurora.dao.ClasificacionDao;
import ec.com.aurora.dao.CommonsDao;
import ec.com.aurora.dto.ClasificacionDTO;
import ec.com.aurora.dto.QClasificacionDTO;

/**
 * @author amaru
 *
 */
@Stateless
public class ClasificacionDaoImpl extends CommonsDao implements ClasificacionDao {

	@Override
	public List<ClasificacionDTO> obtenerClasificacioes() {
		QClasificacionDTO clasificaicon = QClasificacionDTO.clasificacionDTO;
		JPAQuery<ClasificacionDTO> query = new JPAQuery<>(em);
		return query.from(clasificaicon).fetch();
	}

}
