package ec.com.aurora.dao.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;

import ec.com.aurora.dao.ArticuloDao;
import ec.com.aurora.dao.CommonsDao;
import ec.com.aurora.dto.ArticuloDTO;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.dto.ClasificacionDTO;
import ec.com.aurora.dto.QArticuloDTO;
import ec.com.aurora.dto.QCatalogoDTO;
import ec.com.aurora.dto.QClasificacionDTO;

@Stateless
public class ArticuloDaoImpl extends CommonsDao implements ArticuloDao {

	@Override
	public void agregarArticulo(ArticuloDTO articulo) {
		em.persist(articulo);
	}

	@Override
	public void actualizarArticulo(ArticuloDTO articulo) {
		em.merge(articulo);
	}

	@Override
	public List<ArticuloDTO> obtnerArticulos() {
		QArticuloDTO articulo = QArticuloDTO.articuloDTO;
		QCatalogoDTO catalogo = QCatalogoDTO.catalogoDTO;
		QClasificacionDTO clasificacion = QClasificacionDTO.clasificacionDTO;
		JPAQuery<ArticuloDTO> query = new JPAQuery<>(em);
		return query.from(articulo).innerJoin(articulo.clasificacionproducto, clasificacion)
				.innerJoin(articulo.catalogo, catalogo)
				.select(Projections.bean(ArticuloDTO.class, articulo.id, articulo.existencia, articulo.nombreproducto,
						articulo.precio,
						Projections.bean(ClasificacionDTO.class, clasificacion.id, clasificacion.nombreclasificacion)
								.as(articulo.clasificacionproducto),
						Projections.bean(CatalogoDTO.class, catalogo.nombrecatalogo).as(articulo.catalogo)))
				.fetch();
	}

	@Override
	public ArticuloDTO obtnerArticuloPorId(int idArticulo) {
		QArticuloDTO articulo = QArticuloDTO.articuloDTO;
		QClasificacionDTO clasificacion = QClasificacionDTO.clasificacionDTO;
		QCatalogoDTO catalogo = QCatalogoDTO.catalogoDTO;
		JPAQuery<ArticuloDTO> query = new JPAQuery<>(em);
		return query.from(articulo).innerJoin(articulo.clasificacionproducto, clasificacion)
				.innerJoin(articulo.catalogo, catalogo).where(articulo.id.eq(idArticulo))
				.select(Projections.bean(ArticuloDTO.class, articulo.id, articulo.existencia, articulo.nombreproducto,
						articulo.precio,
						Projections.bean(ClasificacionDTO.class, clasificacion.id, clasificacion.nombreclasificacion)
								.as(articulo.clasificacionproducto),
						Projections.bean(CatalogoDTO.class, catalogo.nombrecatalogo).as(articulo.catalogo)))
				.fetchOne();
	}

}
