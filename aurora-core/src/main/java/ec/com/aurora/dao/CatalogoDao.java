/**
 * 
 */
package ec.com.aurora.dao;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.CatalogoDTO;

/**
 * @author amaru
 *
 */
@Local
public interface CatalogoDao {

	List<CatalogoDTO> obtenerCatalogos();

	void crearCatalogos(CatalogoDTO catalogo);

}
