/**
 * 
 */
package ec.com.aurora.dao;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.ClasificacionDTO;

/**
 * @author amaru
 *
 */
@Local
public interface ClasificacionDao {

	List<ClasificacionDTO> obtenerClasificacioes();
	
}
