/**
 * 
 */
package ec.com.aurora.dao;

import java.util.List;

import javax.ejb.Local;

import ec.com.aurora.dto.ArticuloDTO;

/**
 * @author amaru
 *
 */
@Local
public interface ArticuloDao {
	
	void agregarArticulo(ArticuloDTO articulo);
	
	void actualizarArticulo(ArticuloDTO articulo);
	
	List<ArticuloDTO> obtnerArticulos();
	
	ArticuloDTO obtnerArticuloPorId(int idArticulo);

}
