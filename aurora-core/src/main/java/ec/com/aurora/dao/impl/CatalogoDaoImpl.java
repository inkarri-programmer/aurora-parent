/**
 * 
 */
package ec.com.aurora.dao.impl;

import java.util.List;

import javax.ejb.Stateless;

import com.querydsl.jpa.impl.JPAQuery;

import ec.com.aurora.dao.CatalogoDao;
import ec.com.aurora.dao.CommonsDao;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.dto.QCatalogoDTO;

/**
 * @author amaru
 *
 */
@Stateless
public class CatalogoDaoImpl extends CommonsDao implements CatalogoDao {

	@Override
	public List<CatalogoDTO> obtenerCatalogos() {
		QCatalogoDTO catalogo = QCatalogoDTO.catalogoDTO;
		JPAQuery<CatalogoDTO> query = new JPAQuery<>(em);
		return query.from(catalogo).fetch();
	}

	@Override
	public void crearCatalogos(CatalogoDTO catalogo) {
		em.persist(catalogo);
	}

}
