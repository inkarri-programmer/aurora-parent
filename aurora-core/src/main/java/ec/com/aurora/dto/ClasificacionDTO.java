package ec.com.aurora.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author amaru
 *
 */
@Entity
@Table(name = "clasificacionproducto")
public class ClasificacionDTO implements Serializable {

	private static final long serialVersionUID = 8660255844081435696L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "nombreclasificacion", nullable = false, length = 200)
	private String nombreclasificacion;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombreclasificacion() {
		return this.nombreclasificacion;
	}

	public void setNombreclasificacion(String nombreclasificacion) {
		this.nombreclasificacion = nombreclasificacion;
	}

}
