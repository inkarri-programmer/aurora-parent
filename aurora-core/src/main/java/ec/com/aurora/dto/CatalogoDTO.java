package ec.com.aurora.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author amaru
 *
 */
@Entity
@Table(name = "catalogo")
public class CatalogoDTO implements Serializable {

	private static final long serialVersionUID = 4850671566704355655L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "nombrecatalogo", nullable = false, length = 200)
	private String nombrecatalogo;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombrecatalogo() {
		return this.nombrecatalogo;
	}

	public void setNombrecatalogo(String nombrecatalogo) {
		this.nombrecatalogo = nombrecatalogo;
	}

}
