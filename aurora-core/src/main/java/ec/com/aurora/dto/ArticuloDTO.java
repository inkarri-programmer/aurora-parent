package ec.com.aurora.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author amaru
 *
 */
@Entity
@Table(name = "articulo")
public class ArticuloDTO implements Serializable {

	private static final long serialVersionUID = -8596655735711328476L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "idcatalogo", nullable = false)
	private Integer idCatalogo;

	@Column(name = "idclasificacion", nullable = false)
	private Integer idClasificacion;

	private String nombreproducto;

	private BigDecimal precio;

	private BigDecimal existencia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcatalogo", insertable = false, updatable = false)
	private CatalogoDTO catalogo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idclasificacion", insertable = false, updatable = false)
	private ClasificacionDTO clasificacionproducto;

	public Integer getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(Integer idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public Integer getIdClasificacion() {
		return idClasificacion;
	}

	public void setIdClasificacion(Integer idClasificacion) {
		this.idClasificacion = idClasificacion;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CatalogoDTO getCatalogo() {
		return this.catalogo;
	}

	public void setCatalogo(CatalogoDTO catalogo) {
		this.catalogo = catalogo;
	}

	public ClasificacionDTO getClasificacionproducto() {
		return this.clasificacionproducto;
	}

	public void setClasificacionproducto(ClasificacionDTO clasificacionproducto) {
		this.clasificacionproducto = clasificacionproducto;
	}

	@Column(name = "nombreproducto", nullable = false, length = 200)
	public String getNombreproducto() {
		return this.nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	@Column(name = "precio", nullable = false, precision = 5)
	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	@Column(name = "existencia", nullable = false, precision = 131089, scale = 0)
	public BigDecimal getExistencia() {
		return this.existencia;
	}

	public void setExistencia(BigDecimal existencia) {
		this.existencia = existencia;
	}

}
