package ec.com.aurora.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author amaru
 *
 */
@Entity
@Table(name = "ventaarticulo")
public class VentaArticuloDTO implements Serializable {

	private static final long serialVersionUID = -7592360230979961031L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	private ArticuloDTO articulo;

	private String comprador;

	private String email;

	private BigDecimal cantidad;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idarticulo", nullable = false)
	public ArticuloDTO getArticulo() {
		return this.articulo;
	}

	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}

	@Column(name = "comprador", nullable = false, length = 200)
	public String getComprador() {
		return this.comprador;
	}

	public void setComprador(String comprador) {
		this.comprador = comprador;
	}

	@Column(name = "email", nullable = false, length = 200)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "cantidad", nullable = false, precision = 131089, scale = 0)
	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

}
