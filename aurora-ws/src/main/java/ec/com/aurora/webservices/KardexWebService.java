/**
 * 
 */
package ec.com.aurora.webservices;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import ec.com.aurora.commons.exception.AuroraException;
import ec.com.aurora.commons.exception.AuroraStateException;
import ec.com.aurora.dto.ArticuloDTO;
import ec.com.aurora.dto.CatalogoDTO;
import ec.com.aurora.servicio.KardexService;

/**
 * @author amaru
 *
 */
@Path("/kardex")
@Stateless
public class KardexWebService {

	@EJB
	private KardexService kardexService;

	@GET
	@Path("/obtnerArticuloPorId/{idArticulo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtnerArticuloPorId(@PathParam(value = "idArticulo") int idEquipo) {
		ResponseBuilder builder = Response.ok();
		try {
			builder.entity(kardexService.obtnerArticuloPorId(idEquipo));
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@POST
	@Path("/registrarArticulo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrarArticulo(ArticuloDTO articulo) {
		ResponseBuilder builder = Response.ok();
		try {
			kardexService.agregarArticulo(articulo);
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@POST
	@Path("/actualizarArticulo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizarArticulo(ArticuloDTO articulo) {
		ResponseBuilder builder = Response.ok();
		try {
			kardexService.actualizarArticulo(articulo);
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@GET
	@Path("/obtnerArticulos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtnerArticulos() {
		ResponseBuilder builder = Response.ok();
		try {
			builder.entity(kardexService.obtnerArticulos());
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@GET
	@Path("/obtenerCatalogos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerCatalogos() {
		ResponseBuilder builder = Response.ok();
		try {
			builder.entity(kardexService.obtenerCatalogos());
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@POST
	@Path("/crearCatalogos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crearCatalogos(CatalogoDTO catalogo) {
		ResponseBuilder builder = Response.ok();
		try {
			kardexService.crearCatalogos(catalogo);
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

	@GET
	@Path("/obtenerClasificacioes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerClasificacioes() {
		ResponseBuilder builder = Response.ok();
		try {
			builder.entity(kardexService.obtenerClasificacioes());
			builder.header("restype", "SUCCESS");
		} catch (AuroraStateException e) {
			builder.header("restype", "INFO");
			builder.header("message", e.getMessage());
		} catch (AuroraException e) {
			builder.header("restype", "ERROR");
			builder.header("message", e.getMessage());
		}
		return builder.build();
	}

}
